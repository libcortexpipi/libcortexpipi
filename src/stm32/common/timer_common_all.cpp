#include <cortexpipi/tim.hpp>

#include <cortexpipi/helpers/static_array.hpp>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>
#include <stdexcept>
#include <algorithm>





namespace cortexpipi::tim {


namespace _impl{






HandlerRegistryAdvanced _tim1_hr{};
HandlerRegistryRegular _tim2_hr{};
// HandlerRegistryRegular _tim3_hr{};
// #if defined(TIM4_BASE)
// HandlerRegistryRegular _tim4_hr{};
// #endif
// HandlerRegistryRegular _tim5_hr{};
// HandlerRegistryRegular _tim6_hr{};
// HandlerRegistryRegular _tim7_hr{};
// #if defined(TIM8_BASE)
// HandlerRegistryAdvanced _tim8_hr{};
// #endif
// #if defined(TIM9_BASE)
// HandlerRegistryRegular _tim9_hr{};
// #endif
// #if defined(TIM10_BASE)
// HandlerRegistryRegular _tim10_hr{};
// #endif
// #if defined(TIM11_BASE)
// HandlerRegistryRegular _tim11_hr{};
// #endif
// #if defined(TIM12_BASE)
// HandlerRegistryRegular _tim12_hr{};
// #endif
// #if defined(TIM13_BASE)
// HandlerRegistryRegular _tim13_hr{};
// #endif
// #if defined(TIM14_BASE)
// HandlerRegistryRegular _tim14_hr{};
// #endif
// #if defined(TIM15_BASE)
// HandlerRegistryRegular _tim15_hr{};
// #endif
// #if defined(TIM16_BASE)
// HandlerRegistryRegular _tim16_hr{};
// #endif
// #if defined(TIM17_BASE)
// HandlerRegistryRegular _tim17_hr{};
// #endif
// #if defined(TIM21_BASE)
// HandlerRegistryRegular _tim21_hr{};
// #endif
// #if defined(TIM22_BASE)
// HandlerRegistryRegular _tim22_hr{};
// #endif

namespace {
Descriptor _descrsArr[] = {
    {
        .device = TIM1,
        .deviceVerbose = 1,
        .handlers = &_tim1_hr,
        .rccReset = static_cast<uint32_t>(RST_TIM1)
    },
    {
        .device = TIM2,
        .deviceVerbose = 2,
        .handlers = &_tim2_hr,
        .rccReset = static_cast<uint32_t>(RST_TIM1)
    },
//     {
//         .device = TIM3,
//         .deviceVerbose = 3,
//         .handlers = &_tim3_hr
//     },
// #if defined(TIM4_BASE)
//     {
//         .device = TIM4,
//         .deviceVerbose = 4,
//         .handlers = &_tim4_hr
//     },
// #endif
//     {
//         .device = TIM5,
//         .deviceVerbose = 5,
//         .handlers = &_tim5_hr
//     },
//     {
//         .device = TIM6,
//         .deviceVerbose = 6,
//         .handlers = &_tim6_hr
//     },
//     {
//         .device = TIM7,
//         .deviceVerbose = 7,
//         .handlers = &_tim7_hr
//     },
// #if defined(TIM8_BASE)
//     {
//         .device = TIM8,
//         .deviceVerbose = 8,
//         .handlers = &_tim8_hr
//     },
// #endif
// #if defined(TIM9_BASE)
//     {
//         .device = TIM9,
//         .deviceVerbose = 9,
//         .handlers = &_tim9_hr
//     },
// #endif
// #if defined(TIM10_BASE)
//     {
//         .device = TIM10,
//         .deviceVerbose = 10,
//         .handlers = &_tim10_hr
//     },
//
// #endif
// #if defined(TIM11_BASE)
//     {
//         .device = TIM11,
//         .deviceVerbose = 11,
//         .handlers = &_tim11_hr
//     },
// #endif
// #if defined(TIM12_BASE)
//     {
//         .device = TIM12,
//         .deviceVerbose = 12,
//         .handlers = &_tim12_hr
//     },
// #endif
// #if defined(TIM13_BASE)
//     {
//         .device = TIM13,
//         .deviceVerbose = 13,
//         .handlers = &_tim13_hr
//     },
// #endif
// #if defined(TIM14_BASE)
//     {
//         .device = TIM14,
//         .deviceVerbose = 14,
//         .handlers = &_tim14_hr
//     },
// #endif
// #if defined(TIM15_BASE)
//     {
//         .device = TIM15,
//         .deviceVerbose = 15,
//         .handlers = &_tim15_hr
//     },
// #endif
// #if defined(TIM16_BASE)
//     {
//         .device = TIM16,
//         .deviceVerbose = 16,
//         .handlers = &_tim16_hr
//     },
// #endif
// #if defined(TIM17_BASE)
//     {
//         .device = TIM17,
//         .deviceVerbose = 17,
//         .handlers = &_tim17_hr
//     },
// #endif
// #if defined(TIM21_BASE)
//     {
//         .device = TIM21,
//         .deviceVerbose = 21,
//         .handlers = &_tim21_hr
//     },
// #endif
// #if defined(TIM22_BASE)
//     {
//         .device = TIM22,
//         .deviceVerbose = 22,
//         .handlers = &_tim22_hr
//     },
// #endif
};
// helpers::static_array<Descriptor> _descrsPrivate(_descrsArr);


} // namespace anonymous

void HandlerRegistryRegular::set(InterruptType htype, const Callback_t& handler){
    if(htype==InterruptType::SIMPLE) {
        simpleCb = handler;
    }
}
void HandlerRegistryRegular::unset(InterruptType htype){
    if(htype==InterruptType::SIMPLE) {
        simpleCb = std::nullopt;
    }
}
void HandlerRegistryAdvanced::set(InterruptType htype, const Callback_t& handler){
    switch(htype){ // BRK UP TRG CC
        case InterruptType::BREAK:
            breakCb = handler;
            break;
        case InterruptType::UPDATE:
            updateCb = handler;
            break;
        case InterruptType::TRIGGER:
            triggerCb = handler;
            break;
        case InterruptType::CAP_COM:
            capComCb = handler;
            break;
        default: break;
    }
}
void HandlerRegistryAdvanced::unset(InterruptType htype){
    switch(htype){ // BRK UP TRG CC
        case InterruptType::BREAK:
            breakCb = std::nullopt;
            break;
        case InterruptType::UPDATE:
            updateCb = std::nullopt;
            break;
        case InterruptType::TRIGGER:
            triggerCb = std::nullopt;
            break;
        case InterruptType::CAP_COM:
            capComCb = std::nullopt;
            break;
        default: break;
    }
}

DescriptorRegistry::DescriptorRegistry():
    _descrs(_descrsArr)
    // _descrs(nullptr, 0)
{
}

Descriptor * DescriptorRegistry::find(uint8_t tim) {
    for (auto& descr: _descrs) {
        if(descr.deviceVerbose == tim) {
            return &descr;
        }
    }
    return nullptr;
}

} // namespace _impl



Timer::Timer(uint8_t tim):
    _descr(_impl::DescriptorRegistry::instance().find(tim))
{
    cm3_assert(_descr!=nullptr);
    cm3_assert(_descr->handlers!=nullptr);
}

Timer::Timer(const Timer& tim):
    _descr(tim._descr)
{
    cm3_assert(_descr!=nullptr);
    cm3_assert(_descr->handlers!=nullptr);
}

void Timer::setIntCallback(GenericInterrupt_t interrupt, const CallbackType& cb){
    _descr->handlers->set(interrupt, cb);
}

void Timer::removeInterruptCallback(GenericInterrupt_t interrupt){
    _descr->handlers->unset(interrupt);
}

void Timer::setClockDiv(ClockDiv value){
    uint32_t regValue = static_cast<uint32_t>(value) << 8;
    regValue &= TIM_CR1_CKD_CK_INT_MASK;
	TIM_CR1(_descr->device) &= ~TIM_CR1_CKD_CK_INT_MASK;
	TIM_CR1(_descr->device) |= regValue;
}

void Timer::enableInterrupt(Interrupt_t interrupt){
    TIM_DIER(_descr->device) |= static_cast<uint32_t>(interrupt);
}
void Timer::disableInterrupt(Interrupt_t interrupt){
    TIM_DIER(_descr->device) &= ~static_cast<uint32_t>(interrupt);
}

void Timer::reset() {
    rcc_periph_reset_pulse(static_cast<rcc_periph_rst>(_descr->rccReset));
}

void Timer::setDirection(Direction dir) {
    uint32_t reg = dir&TIM_CR1_DIR_DOWN;
    TIM_CR1(_descr->device) &= ~TIM_CR1_DIR_DOWN;
    TIM_CR1(_descr->device) |= reg;
}

void Timer::setAlignment(Alignment al) {
    uint32_t alignment = (static_cast<uint32_t>(al)<<5) & TIM_CR1_CMS_MASK;
	TIM_CR1(_descr->device) &= ~TIM_CR1_CMS_MASK;
	TIM_CR1(_descr->device) |= alignment;
}

void Timer::enablePreload() {
    TIM_CR1(_descr->device) |= TIM_CR1_ARPE;
}

void Timer::disablePreload() {
    TIM_CR1(_descr->device) &= ~TIM_CR1_ARPE;
}

void Timer::setCounter(uint32_t cnt) {
    TIM_CNT(_descr->device) = cnt;
}

uint32_t Timer::getCounter() const {
    return TIM_CNT(_descr->device);
}

void Timer::setPrescaler(uint32_t psc) {
    TIM_PSC(_descr->device) = psc;
}

uint32_t Timer::getPrescaler() const {
    return TIM_PSC(_descr->device);
}

void Timer::setPeriod(uint32_t period) {
    TIM_ARR(_descr->device) = period;
}

uint32_t Timer::getPeriod() const {
    return TIM_ARR(_descr->device);
}

void Timer::continiousMode(bool state) {
    uint32_t reg = state?0x0:TIM_CR1_OPM;
    TIM_CR1(_descr->device) &= ~TIM_CR1_OPM;
    TIM_CR1(_descr->device) |= reg;
}

void Timer::start() {
    TIM_CR1(_descr->device) |= TIM_CR1_CEN;
}

void Timer::stop() {
    TIM_CR1(_descr->device) &= ~TIM_CR1_CEN;
}

bool Timer::getFlag(EventFlag f) {
    return (TIM_SR(_descr->device) & static_cast<uint32_t>(f));
}
void Timer::clearFlag(EventFlag f) {
    TIM_SR(_descr->device) &= ~static_cast<uint32_t>(f);
}

} // namespace cortexpipi::tim

using namespace cortexpipi::tim::_impl;


inline void tryRunCallback(std::optional<HandlerRegistry::Callback_t>& hr) {
    if(hr){
        (*hr)();
    }
}

// TODO: implement other timers, ignore undefined
void tim1_brk_isr(void) {
    tryRunCallback(_tim1_hr.breakCb);
}
void tim1_up_isr(void) {
    tryRunCallback(_tim1_hr.updateCb);
}
void tim1_trg_com_isr(void) {
    tryRunCallback(_tim1_hr.triggerCb);
}
void tim1_cc_isr(void) {
    tryRunCallback(_tim1_hr.capComCb);
}
#include <cortexpipi/gpio.hpp>
// extern "C" {
void tim2_isr(void) {
    tryRunCallback(_tim2_hr.simpleCb);
    // if (timer_get_flag(TIM2, TIM_SR_UIF)) {
    //     timer_clear_flag(TIM2, TIM_SR_UIF);
    //     cortexpipi::GpioPin led(0, 7);
    //     led.toggle();
    //
    // }
}
// }
