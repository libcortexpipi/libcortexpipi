#include <cortexpipi/spi.hpp>
#include <libopencmsis/core_cm3.h>
#include <libopencm3/stm32/spi.h>


namespace cortexpipi{

namespace{
const uint32_t portMapping_[] = {
  SPI1,
  SPI2,
  SPI3
};
const uint32_t portMappingSize_ = sizeof(portMapping_)/sizeof(uint32_t);
}
SPI::SPI(uint8_t port):
  rcc_periph_module_simple(portMapping_[port])
{
    if (port >= portMappingSize_) {
      // throw std::out_of_range("Gpio port/pin out of range");
      hard_fault_handler();
    }
}
}
