#include <cortexpipi/gpio.hpp>
#include <libopencm3/stm32/gpio.h>




namespace cortexpipi {

void GpioPin::set(bool value) {
    GPIO_BSRR(this->devnum()) = value ? _pin : (_pin<<16);
}

void GpioPin::toggle() {
    uint32_t odr = GPIO_ODR(this->devnum());
	GPIO_BSRR(this->devnum()) = ((odr & _pin) << 16) | (~odr & _pin);
}

bool GpioPin::read() {
    return (GPIO_IDR(this->devnum())&_pin)!=0;
}

}
