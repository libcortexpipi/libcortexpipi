#include <cortexpipi/gpio.hpp>

#include <libopencm3/stm32/gpio.h>
#include <libopencmsis/core_cm3.h>
// #include <stdexcept>
namespace cortexpipi {

namespace {
  const uint32_t _portMapping[] = {
    GPIOA,
    GPIOB,
    GPIOC,
    GPIOD,
    GPIOE,
    GPIOF,
    GPIOG,
    GPIOH,
    GPIOI,
    GPIOJ
  };
  const uint32_t _portMappingSize = sizeof(_portMapping)/sizeof(uint32_t);
}


GpioPin::GpioPin (uint8_t port, uint8_t pin):
rcc_periph_module_simple(_portMapping[port]),
_pin(1<<pin),
_portNumber (port),
_pinNumber(pin)
{}

void GpioPin::configure(const Mode m){
  uint32_t mode;
  switch(m) {
    case IN: {
      mode = GPIO_MODE_INPUT;
      break;
    }
    case OUT: {
      mode = GPIO_MODE_OUTPUT;
      break;
    }
    case ANAL: {
      mode = GPIO_MODE_ANALOG;
      break;
    }
    case AF: {
      mode = GPIO_MODE_AF;
      break;
    }
    default: return;
  }
  gpio_mode_setup(this->devnum(), mode, GPIO_PUPD_NONE, _pin);
  if(mode == GPIO_MODE_OUTPUT) {
    gpio_set_output_options(this->devnum(), GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, _pin);
  }
}

void GpioPin::configureAF(uint8_t af) {
  gpio_set_af(this->devnum(), af, _pin);
}


}
