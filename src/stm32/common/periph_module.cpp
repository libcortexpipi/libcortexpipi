#include <cortexpipi/periph_module.hpp>
#include <cortexpipi/helpers/static_array.hpp>
#include <algorithm>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/spi.h>



namespace cortexpipi {
namespace {
std::pair<uint32_t, uint32_t> rccs_raw[] = {
    {ADC1, RCC_ADC1},
    {ADC2, RCC_ADC2},
    {ADC3, RCC_ADC3},
    {GPIOA, RCC_GPIOA},
    {GPIOB, RCC_GPIOB},
    {GPIOC, RCC_GPIOC},
    {GPIOD, RCC_GPIOD},
    {GPIOE, RCC_GPIOE},
    {GPIOF, RCC_GPIOF},
#if defined(GPIOG) && defined(RCC_GPIOG)
    {GPIOG, RCC_GPIOG},
#endif
#if defined(GPIOH) && defined(RCC_GPIOH)
    {GPIOH, RCC_GPIOH},
#endif
#if defined(GPIOI) && defined(RCC_GPIOI)
    {GPIOI, RCC_GPIOI},
#endif
    {SPI1, RCC_SPI1},
    {SPI2, RCC_SPI2},
    {SPI3, RCC_SPI3},

};
helpers::static_array<std::pair<uint32_t, uint32_t>> rccs {rccs_raw};
}
uint32_t getRccByDev(uint32_t dev) {
    auto res = std::find_if(rccs.begin(), rccs.end(), [&](const auto& kv){
        return kv.first == dev;
    });
    return (*res).second;
}

void rcc_periph_module_base::clock_enable() {
    rcc_periph_clock_enable(static_cast<rcc_periph_clken>(_rcc_number));
}

void rcc_periph_module_base::clock_disable() {
    rcc_periph_clock_disable(static_cast<rcc_periph_clken>(_rcc_number));
}

}
