
#include <cortexpipi/tim.hpp>
#include <libopencm3/stm32/rcc.h>


namespace cortexpipi::tim{

uint32_t Timer::getBaseFrequency(){
    switch(_descr->deviceVerbose){
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 12:
        case 13:
        case 14:
            return rcc_apb1_frequency*2;
        case 1:
        case 8:
        case 9:
        case 10:
        case 11:
            return rcc_apb2_frequency*2;
        default:
            cm3_assert_not_reached();

    }
}

}
