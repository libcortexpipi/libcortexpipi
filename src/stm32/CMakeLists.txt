

set(STM32_COMMON_DIR ${CMAKE_CURRENT_SOURCE_DIR}/common)
set(STM32_DIR ${CMAKE_CURRENT_SOURCE_DIR})

add_subdirectory(common)

add_subdirectory(f0)
add_subdirectory(f1)
add_subdirectory(f2)
add_subdirectory(f3)
add_subdirectory(f4)
add_subdirectory(f7)

add_subdirectory(g0)
add_subdirectory(g4)

add_subdirectory(h7)

add_subdirectory(l0)
add_subdirectory(l1)
add_subdirectory(l4)
