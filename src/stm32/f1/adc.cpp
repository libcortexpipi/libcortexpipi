
#include <cortexpipi/adc.hpp>
#include <libopencm3/stm32/adc.h>

namespace cortexpipi {
namespace adc {

namespace {
struct _AnalogPin {
  uint8_t pin; /// GPIO pin number (6 for PA6, 9 for PB9, etc)
  Channel chan; /// ADC channel
};
struct _AnalogPort {
  uint8_t port; /// GPIO port (0 for GPIOA, 1 for GPIOB, etc)
  const _AnalogPin * pins;
  const size_t pinsSize; /// number of elements in pins
};

const _AnalogPin _pinsPortA[] = {
  {
    .pin = 0, // PA0
    .chan = { .devmask = ADC_DEV1|ADC_DEV2|ADC_DEV3, .channel = 0 }
  },
  {
    .pin = 1, // PA1
    .chan = { .devmask = ADC_DEV1|ADC_DEV2|ADC_DEV3, .channel = 1 }
  },
  {
    .pin = 2, // PA2
    .chan = { .devmask = ADC_DEV1|ADC_DEV2|ADC_DEV3, .channel = 2 }
  },
  {
    .pin = 3, // PA3
    .chan = { .devmask = ADC_DEV1|ADC_DEV2|ADC_DEV3, .channel = 3 }
  },
  {
    .pin = 4, // PA4
    .chan = { .devmask = ADC_DEV1|ADC_DEV2, .channel = 4 }
  },
  {
    .pin = 5, // PA5
    .chan = { .devmask = ADC_DEV1|ADC_DEV2, .channel = 5 }
  },
  {
    .pin = 6, // PA6
    .chan = { .devmask = ADC_DEV1|ADC_DEV2, .channel = 6 }
  },
  {
    .pin = 7, // PA7
    .chan = { .devmask = ADC_DEV1|ADC_DEV2, .channel = 7 }
  }
};

const _AnalogPin _pinsPortB[] = {
  {
    .pin = 0, // PB0
    .chan = { .devmask = ADC_DEV1|ADC_DEV2, .channel = 8 }
  },
  {
    .pin = 1, // PB1
    .chan = { .devmask = ADC_DEV1|ADC_DEV2, .channel = 9 }
  }
};

const _AnalogPin _pinsPortC[] = {
  {
    .pin = 0, // PC0
    .chan = { .devmask = ADC_DEV1|ADC_DEV2|ADC_DEV3, .channel = 10 }
  },
  {
    .pin = 1, // PC1
    .chan = { .devmask = ADC_DEV1|ADC_DEV2|ADC_DEV3, .channel = 11 }
  },
  {
    .pin = 2, // PC2
    .chan = { .devmask = ADC_DEV1|ADC_DEV2|ADC_DEV3, .channel = 12 }
  },
  {
    .pin = 3, // PC3
    .chan = { .devmask = ADC_DEV1|ADC_DEV2|ADC_DEV3, .channel = 13 }
  },
  {
    .pin = 4, // PC4
    .chan = { .devmask = ADC_DEV1|ADC_DEV2, .channel = 14 }
  },
  {
    .pin = 5, // PC5
    .chan = { .devmask = ADC_DEV1|ADC_DEV2, .channel = 15 }
  }
};

const _AnalogPin _pinsPortF[] = {
  {
    .pin = 6, // PF6
    .chan = { .devmask = ADC_DEV3, .channel = 4 }
  },
  {
    .pin = 7, // PF7
    .chan = { .devmask = ADC_DEV3, .channel = 5 }
  },
  {
    .pin = 8, // PF8
    .chan = { .devmask = ADC_DEV3, .channel = 6 }
  },
  {
    .pin = 9, // PF9
    .chan = { .devmask = ADC_DEV3, .channel = 7 }
  },
  {
    .pin = 10, // PF10
    .chan = { .devmask = ADC_DEV3, .channel = 8 }
  }
};

const _AnalogPort _ports[] = {
  {
    .port = 0, // GPIOA
    .pins = _pinsPortA,
    .pinsSize = sizeof(_pinsPortA)/sizeof(_AnalogPin)
  },
  {
    .port = 1, // GPIOB
    .pins = _pinsPortB,
    .pinsSize = sizeof(_pinsPortB)/sizeof(_AnalogPin)
  },
  {
    .port = 2, // GPIOC
    .pins = _pinsPortC,
    .pinsSize = sizeof(_pinsPortC)/sizeof(_AnalogPin)
  },
  {
    .port = 5, // GPIOF
    .pins = _pinsPortF,
    .pinsSize = sizeof(_pinsPortF)/sizeof(_AnalogPin)
  }
};

const size_t _portsSize = sizeof(_ports)/sizeof(_AnalogPort);

} // namespace {

std::optional<Channel> getChannelByGpio(const GpioPin& pin) {
  std::optional<Channel> ret = std::nullopt;
  const auto requiredPort = pin.port();
  const auto requiredPin = pin.pin();
  for (size_t i = 0; i<_portsSize && _ports[i].port<=requiredPort; ++i) {
    if(requiredPort!=_ports[i].port){
      continue;
    }
    auto& port = _ports[i];
    auto pins = port.pins;
    for(size_t j = 0; j<port.pinsSize && pins[j].pin<=requiredPin; ++j) {
      if(pins[j].pin==requiredPin){
        ret = pins[j].chan;
        return ret;
      }
    }
  }
  return ret;
}

uint32_t getAdcByMask(uint8_t mask){
  if (mask&ADC_DEV1) {
    return ADC1;
  }
  if (mask&ADC_DEV2) {
    return ADC2;
  }
  if (mask&ADC_DEV3) {
    return ADC_DEV3;
  }
  return 0;
}



} // namespace adc

ADC::ADC(uint32_t dev):
    rcc_periph_module_simple(dev)
{

}

void ADC::power_on() {
    adc_power_on(this->devnum());
}

void ADC::power_off() {
    adc_power_off(this->devnum());
}

void ADC::power(bool state) {
    if(state) {
        this->power_on();
    } else {
        this->power_off();
    }
}

void ADC::calibrate() {
    adc_reset_calibration(this->devnum());
    adc_calibrate(this->devnum());
}

} // namespace cortexpipi
