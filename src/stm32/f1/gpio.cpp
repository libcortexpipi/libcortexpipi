#include <cortexpipi/gpio.hpp>

#include <libopencm3/stm32/gpio.h>
#include <libopencmsis/core_cm3.h>
// #include <stdexcept>
namespace cortexpipi {

namespace {
  const uint32_t _portMapping[] = {
    GPIOA,
    GPIOB,
    GPIOC,
    GPIOD,
    GPIOE,
    GPIOF,
    GPIOG
  };
  const uint32_t _portMappingSize = sizeof(_portMapping)/sizeof(uint32_t);
} // namespace {

GpioPin::GpioPin (uint8_t port, uint8_t pin):
rcc_periph_module_simple(_portMapping[port]),
_pin(1<<pin),
_portNumber (port),
_pinNumber(pin)
{}

void GpioPin::configure(const Mode m){
  uint32_t cnf, mode;
  switch(m) {
    case IN: {
      cnf = GPIO_CNF_INPUT_FLOAT;
      mode = GPIO_MODE_INPUT;
      break;
    }
    case OUT: {
      cnf = GPIO_CNF_OUTPUT_PUSHPULL;
      mode = GPIO_MODE_OUTPUT_50_MHZ;
      break;
    }
    case ANAL: {
      cnf = GPIO_CNF_INPUT_FLOAT;
      mode = GPIO_MODE_INPUT;
      break;
    }
    case AF: {
      cnf = GPIO_CNF_OUTPUT_ALTFN_PUSHPULL;
      mode = GPIO_MODE_OUTPUT_50_MHZ;
      break;
    }
    default: return;
  }
  gpio_set_mode(this->devnum(), mode, cnf, _pin);
}


}
