#include <cortexpipi/systick.hpp>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/cm3/systick.h>

extern "C" {
  void sys_tick_handler(void) {
    static auto& systick = cortexpipi::Systick::instance();
    ++systick._counter;
  }
}



namespace cortexpipi {
  void Systick::init() {
    systick_counter_disable();
    systick_interrupt_disable();
    systick_set_frequency(1000, rcc_ahb_frequency);
    systick_interrupt_enable();
    /* Start counting. */
    systick_counter_enable();
  }

  void Systick::delay(uint32_t ms) {
    uint32_t start = _counter;
    while((_counter - start) < ms){
      __asm("nop");
    }
  }
}
