#pragma once

#include <cstdint>
#include <cortexpipi/periph_module.hpp>

namespace cortexpipi {
class SPI: public rcc_periph_module_simple {
public:

  enum Mode: uint8_t{
    MASTER,
    SLAVE,
    MASTER_TX_ONLY
  };

  SPI(uint8_t port);

  void configure(const Mode m);

  void write8(const uint8_t data);
  void write16(const uint16_t data);
  uint8_t read8();
  uint16_t read16();
  bool ready();
  bool tx_done();

};
}
