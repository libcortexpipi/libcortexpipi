#pragma once


namespace cortexpipi::helpers {

template<class T>
class Singleton {
public:
  Singleton(const Singleton&) = delete;
  Singleton& operator=(const Singleton&) = delete;
  static T& instance () {
    static T _inst;
    return _inst;
  }
protected:
  Singleton() = default;
};

}
