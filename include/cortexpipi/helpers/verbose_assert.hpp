#ifdef NDEBUG
    #undef NDEBUG
    #define _NDEBUG
#endif
#ifndef CM3_ASSERT_VERBOSE
    #define CM3_ASSERT_VERBOSE
    #define _CM3_ASSERT_VERBOSE
#endif

#include <libopencm3/cm3/assert.h>

#ifdef _NDEBUG
    #undef _NDEBUG
    #define NDEBUG
#endif
#ifdef _CM3_ASSERT_VERBOSE
    #undef _CM3_ASSERT_VERBOSE
    #undef CM3_ASSERT_VERBOSE
#endif
