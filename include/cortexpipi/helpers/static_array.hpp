#pragma once

#include <cortexpipi/helpers/verbose_assert.hpp>

#include <iterator>

namespace cortexpipi::helpers{

template<typename T>
class static_array{
private:
    T* _storage;
    const size_t _len;
public:
    template <typename IT>
    class iterator_impl {
    private:
        IT * _addr;
    public:
        using iterator_category = std::random_access_iterator_tag;
        using value_type = IT;
        using difference_type = std::intptr_t;
        using pointer = value_type*;
        using reference = value_type&;
        using this_type = iterator_impl<value_type>;
        using this_reference = this_type&;

        explicit iterator_impl(pointer addr): _addr(addr){};
        iterator_impl& operator++() {++_addr; return *this;}
        iterator_impl operator++(int) {iterator_impl retval = *this; ++(*this); return retval;}

        iterator_impl& operator--() {--_addr; return *this;}
        iterator_impl operator--(int) {iterator_impl retval = *this; --(*this); return retval;}

        bool operator==(const this_reference other) const {return _addr == other._addr;}
        bool operator!=(const this_reference other) const {return _addr != other._addr;}
        size_t operator-(const this_reference rhs){
            return this->_addr - rhs._addr;
        }
        const IT& operator*() const {return *_addr;}
        IT& operator*() {return *_addr;}

    };
    using iterator = iterator_impl<T>;
    using const_iterator = iterator_impl<const T>;

    static_array(T* storage, size_t len):
        _storage(storage),
        _len(len)
    {
        cm3_assert(_storage!=nullptr);
    }


    static_array(const static_array<T>& in):
        _storage(in._storage),
        _len(in._len)
    {}

    template<std::size_t len>
    constexpr static_array(T(&storage)[len]): _storage(storage), _len(len) {}


    size_t size() const {
        return _len;
    }

    T& operator[](size_t pos) {
        cm3_assert(pos<_len);
        return _storage[pos];
    }

    const T& operator[](size_t pos) const {
        cm3_assert(pos<_len);
        return _storage[pos];
    }

    //----------- ITERATORS -----------

    iterator begin() {
        return iterator(_storage);
    }

    iterator end() {
        return iterator(_storage + _len);
    }

    const_iterator cbegin() const {
        return const_iterator(_storage);
    }

    const_iterator cend() {
        return const_iterator(_storage + _len);
    }



};


} // namespace helpers
