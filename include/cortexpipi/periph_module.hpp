#pragma once

#include <cstdint>

namespace cortexpipi {

uint32_t getRccByDev(uint32_t dev);

class periph_module {
protected:
    const uint32_t _devnum;
public:
    periph_module(uint32_t dev): _devnum(dev) {}
    inline uint32_t devnum() const { return _devnum; }
}; // class periph_module_base

class rcc_periph_module_base: public periph_module {
protected:
    const uint32_t _rcc_number;
public:
    rcc_periph_module_base(uint32_t dev, uint32_t rccnum):
        periph_module(dev), _rcc_number(rccnum) {}
    inline uint32_t rcc_number() const { return _rcc_number; }

    void clock_enable();
    void clock_disable();
}; // class rcc_periph_module_base

class rcc_periph_module_simple : public rcc_periph_module_base {
public:
    rcc_periph_module_simple(uint32_t dev):
        rcc_periph_module_base(dev, getRccByDev(dev)) {}
};


} // namespace cortexpipi
