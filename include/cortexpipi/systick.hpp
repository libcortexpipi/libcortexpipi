#pragma once
#include <cstdint>

#include <cortexpipi/helpers/singleton.hpp>
extern "C" {
  extern void sys_tick_handler();
}


namespace cortexpipi {
class Systick: public helpers::Singleton<Systick> {
public:
  Systick(): _counter(0){}
  void init();
  void delay(uint32_t ms);
protected:
  volatile uint32_t _counter;
  friend void ::sys_tick_handler();
};
}
