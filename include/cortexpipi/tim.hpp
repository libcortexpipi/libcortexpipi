#pragma once

#include <cstdint>
#include <cstddef>

#include <libopencm3/stm32/timer.h>
#include <cortexpipi/impl/tim.hpp>


namespace cortexpipi::tim {
class Timer{
private:
    // real device (TIM1, TIM2, etc)
    _impl::Descriptor* _descr;

public:
    Timer(uint8_t tim);
    Timer(const Timer& tim);

    // TODO: interrupt callbacks API

    using GenericInterrupt_t = _impl::HandlerRegistry::InterruptType;
    using CallbackType = _impl::HandlerRegistry::Callback_t;

    void setIntCallback(GenericInterrupt_t interrupt, const CallbackType& cb);
    void removeInterruptCallback(GenericInterrupt_t interrupt);

    enum Interrupt_t: uint32_t {
        TRIGGER_DMAREQ = TIM_DIER_TDE,
        COMPARE_DMAREQ = TIM_DIER_COMDE,
        CAP_1_DMAREQ = TIM_DIER_CC1DE,
        CAP_2_DMAREQ = TIM_DIER_CC2DE,
        CAP_3_DMAREQ = TIM_DIER_CC3DE,
        CAP_4_DMAREQ = TIM_DIER_CC4DE,
        UPDATE_DMAREQ = TIM_DIER_UDE,
        BREAK_INT = TIM_DIER_BIE,
        TRIGGER_INT = TIM_DIER_TIE,
        COMPARE_INT = TIM_DIER_COMIE,
        CAP_1_INT = TIM_DIER_CC1IE,
        CAP_2_INT = TIM_DIER_CC2IE,
        CAP_3_INT = TIM_DIER_CC3IE,
        CAP_4_INT = TIM_DIER_CC4IE,
        UPDATE_INT = TIM_DIER_UIE
    };
    void enableInterrupt(Interrupt_t interrupt);
    void disableInterrupt(Interrupt_t interrupt);


    enum EventFlag: uint32_t {
        CAP_1_OVERCAP = TIM_SR_CC1OF,
        CAP_2_OVERCAP = TIM_SR_CC2OF,
        CAP_3_OVERCAP = TIM_SR_CC3OF,
        CAP_4_OVERCAP = TIM_SR_CC4OF,
        BREAK = TIM_SR_BIF,
        TRIGGER = TIM_SR_TIF,
        COMPARE = TIM_SR_COMIF,
        CAP_1 = TIM_SR_CC1IF,
        CAP_2 = TIM_SR_CC2IF,
        CAP_3 = TIM_SR_CC3IF,
        CAP_4 = TIM_SR_CC4IF,
        UPDATE = TIM_SR_UIF
    };

    bool getFlag(EventFlag f);
    void clearFlag(EventFlag f);

    enum ClockDiv: uint32_t{
        DIV0 = 0x0,
        DIV2 = 0x1,
        DIV4 = 0x2
    };
    void setClockDiv(ClockDiv value);

    enum Size {
        S8,
        S16,
        S32
    };
    void enableAutoPreload();
    void disableAutoPreload();
    void setPreload(uint32_t preload);
    Size getPreloadSize() const;

    void start();
    void stop();

    enum Direction {
        UP = 0x0,
        DOWN = 0x1
    };

    void setDirection(Direction dir);


    enum Alignment {
        EDGE = 0,
        CENTER_UP = 0x1,
        CENTER_DOWN = 0x2,
        CENTER_BOTH = 0x3
    };

    void setAlignment(Alignment al);

    /**
     * reset timer
     */
    void reset();

    /**
     * enable/disable preload
     * if enabled, counter value will be loaded on update event
     * if disabled, counter will be updated immediately
     */
    void enablePreload();
    void disablePreload();


    void setPrescaler(uint32_t psc);
    uint32_t getPrescaler() const;

    void setCounter(uint32_t counter);
    uint32_t getCounter() const;

    void setPeriod(uint32_t period);
    uint32_t getPeriod() const;

    /**
     * if true then continious mode
     * if false then one pulse mode
     */
    void continiousMode(bool state = true);

    uint32_t getBaseFrequency();
};

class PwmPin{
private:
    Timer _tim;
    const uint8_t _channel;

public:
    PwmPin(const Timer& tim, uint8_t channel);
    PwmPin(uint8_t tim, uint8_t channel);
    PwmPin(const PwmPin& pin);

    const Timer& getTimer() const;
    uint8_t getChannel() const;

    /**
     * set value from 0 to uint32_max (0xffffffff)
     * max means "always enabled", 0 means "always disabled".
     * Precision can be different on various timers and MCUs,
     * also it depends on preload value.
     */
    void set(uint32_t value);

    /**
     * set value from 0.0 to 1.0
     * 1.0 means "always enabled", 0.0 means "always disabled"
     * Precision can be different on various timers and MCUs,
     * also it depends on preload value.
     */
    void set(float value);


};


} // namespace cortexpipi::tim
