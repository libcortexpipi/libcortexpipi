#pragma once

#include <cstdint>
#include <cstddef>
#include <optional>
#include <cortexpipi/gpio.hpp>
#include <cortexpipi/periph_module.hpp>

namespace cortexpipi {
namespace adc {

enum ADC_DEVMASK: uint8_t {
    ADC_DEV1 = 1<<0,
    ADC_DEV2 = 1<<1,
    ADC_DEV3 = 1<<2
};

struct Channel {
  /**
   * ADC mask.
   * @ref ADC_DEVMASK
   */
  uint8_t devmask;
  uint8_t channel;
};


uint32_t getAdcByNumber(uint8_t number);

/**
 * Get ADC device (ADC1, ADC2 or ADC3) by mask @ref ADC_DEVMASK
 * @return ADC device or zero
 */
uint32_t getAdcByMask(uint8_t mask);

std::optional<Channel> getChannelByGpio(const GpioPin& pin);

} // namespace adc

class ADC: public rcc_periph_module_simple {
public:
    /***
     * @param dev - ADC device number (ADC1, ADC2, ADC3)
     */
    ADC(uint32_t dev);

    void power_on();
    void power_off();
    void power(bool state);
    void calibrate();
};

} // namespace cortexpipi
