#pragma once


#include <cortexpipi/helpers/singleton.hpp>
#include <cortexpipi/helpers/static_array.hpp>

#include <functional>
#include <optional>

namespace cortexpipi::tim::_impl{


struct Handlers;

struct HandlerRegistry{
    using Callback_t = std::function<void()>;
    // using Callback_t = void(*)(void);
    enum InterruptType: uint8_t{
        SIMPLE = 1,
        BREAK = 2,
        UPDATE = SIMPLE,
        TRIGGER = 3,
        CAP_COM = 4
    };
    virtual void set(InterruptType htype, const Callback_t& handler) = 0;
    virtual void unset(InterruptType htype) = 0;
    virtual ~HandlerRegistry(){};
};

struct HandlerRegistryRegular: public HandlerRegistry {
    std::optional<Callback_t> simpleCb;
    virtual void set(InterruptType htype, const Callback_t& handler);
    virtual void unset(InterruptType htype);
    virtual ~HandlerRegistryRegular(){};
};

struct HandlerRegistryAdvanced: public HandlerRegistry {
    std::optional<Callback_t> breakCb;
    std::optional<Callback_t> updateCb;
    std::optional<Callback_t> triggerCb;
    std::optional<Callback_t> capComCb;
    virtual void set(InterruptType htype, const Callback_t& handler);
    virtual void unset(InterruptType htype);
    virtual ~HandlerRegistryAdvanced(){};
};

struct Descriptor{
    uint32_t device;
    uint8_t deviceVerbose;
    HandlerRegistry * handlers;
    uint32_t rccReset;
};


class DescriptorRegistry: public helpers::Singleton<DescriptorRegistry> {
private:
    helpers::static_array<Descriptor> _descrs;
public:
    DescriptorRegistry();
    Descriptor* find(uint8_t tim);
};
}
