#pragma once

#include <cstdint>
#include <cortexpipi/periph_module.hpp>

namespace cortexpipi {
class GpioPin: public rcc_periph_module_simple {
private:
  const uint32_t _pin;
  const uint8_t _portNumber;
  const uint8_t _pinNumber;

public:

  enum Mode: uint8_t{
    IN,
    OUT,
    ANAL,
    AF
  };

  GpioPin (uint8_t port, uint8_t pin);
  GpioPin(const GpioPin& inp):
    rcc_periph_module_simple(inp.devnum()),
    _pin(inp._pin),
    _portNumber(inp._portNumber),
    _pinNumber(inp._pinNumber)
  {}

  inline uint8_t port() const { return _portNumber; }
  inline uint8_t pin() const { return _pinNumber; }
  void set(bool value);
  void toggle();
  void configure(const Mode m);
  bool read();
#ifdef HAVE_GPIO_AF
  void configureAF(const uint8_t af);
#endif
};
}
