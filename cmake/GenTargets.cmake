

set(
  CORTEXPIPI_TARGETS
  stm32/f0
  stm32/f1
  stm32/f2
  stm32/f3
  stm32/f4
  stm32/f7
  stm32/l0
  stm32/l1
  stm32/l4
  stm32/g0
  stm32/g4
  stm32/h7
  gd32/f1x0
  lpc13xx
  lpc17xx
  lpc43xx/m4
  lpc43xx/m0
  lm3s
  lm4f
  msp432/e4
  efm32/tg
  efm32/g
  efm32/lg
  efm32/gg
  efm32/hg
  efm32/wg
  efm32/ezr32wg
  sam/3a
  sam/3n
  sam/3s
  sam/3u
  sam/3x
  sam/4l
  sam/d
  vf6xx
  swm050
  pac55xx
)

foreach(TARG IN ITEMS ${CORTEXPIPI_TARGETS})
  string(REPLACE "/" "" TARG_NAME ${TARG})
  string(TOUPPER ${TARG_NAME} TARG_NAME)
  list(APPEND CORTEXPIPI_PLATFORMS ${TARG_NAME})
  set(TARG_IRQ_FILE_REL ./include/libopencm3/${TARG}/irq.json)
  set(TARG_IRQ_FILE_ABS ${PROJECT_SOURCE_DIR}/include/libopencm3/${TARG}/irq.json)
  set(TARG_NVIC_FILE_ABS ${PROJECT_SOURCE_DIR}/include/libopencm3/${TARG}/nvic.h)
  set(TARG_VECT_FILE_ABS ${PROJECT_SOURCE_DIR}/src/${TARG}/vector_nvic.c)
  if(EXISTS ${TARG_IRQ_FILE_ABS})
    add_custom_target(
      ${TARG_NAME}_NVIC_H
      BYPRODUCTS ${TARG_NVIC_FILE_ABS} ${TARG_VECT_FILE_ABS}
      WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
      COMMAND ./scripts/irq2nvic_h ${TARG_IRQ_FILE_REL}
      COMMENT "Generate IRQ vectors for ${TARG_NAME}"
      SOURCES ${TARG_IRQ_FILE_ABS}
    )
  endif()
endforeach()
