

include_directories(${PROJECT_SOURCE_DIR}/include)
if (PIPI_USE_EXCEPTIONS)
    message(STATUS "pipi: cortexpipi will use excepions")
    add_definitions(-D_PIPI_USE_EXCEPTIONS=1)
    set(PIPI_CXXFLAGS ${PIPI_CXXFLAGS} -D_PIPI_USE_EXCEPTIONS=1 PARENT_SCOPE)
endif ()

set(CMAKE_CXX_FLAGS "-fno-use-cxa-atexit ${CMAKE_CXX_FLAGS}")
